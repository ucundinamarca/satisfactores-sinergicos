/**
 * @author Edilson Laverde Molina
 * @email edilsonlaverde_182@hotmail.com
 * @create date 2020-12-12 18:45:48
 * @modify date 2020-12-18 09:48:07
 * @desc Videos REA3
 */
var videos = [{
    "id": "Rjo2aU17qi8",
    "titulo": "",
    "descripcion": "",
    "url": "Rjo2aU17qi8",
    "preguntas": [
        {
            "second": "23",
            "question": "¿Qué consideras que es la carencia?",
            "answers": ["Falta, ausencia o privación de algo", "Combinación de actividades humanas", "Disminución de recursos económicos"],
            "correct": 1,
            "msg-correct": "Muy bien!!",
            "msg-incorrect": "Te sugerimos repasar el video!!"
        },
        {
            "second": "35",
            "question": "¿Qué características tienen las necesidades humanas?",
            "answers": ["Situaciones sociales permanentes", "Son finitas, pocas, cuantificables, fundamentales, son atributos esenciales que se relacionan con la evolución", "Capacidades socio económicas de las comunidades"],
            "correct": 2,
            "msg-correct": "Muy bien!!",
            "msg-incorrect": "Te sugerimos repasar el video!!"
        },
        {
            "second": "50",
            "question": "¿Qué consideras que son los satisfactores?",
            "answers": ["Momentos de la evolución, involucrados en el bienestar humano", "Son formas de ser, tener, hacer y estar, de carácter individual y colectivo, conducentes a la actualización de necesidades.", "Placeres momentáneos de una sociedad"],
            "correct": 2,
            "msg-correct": "Muy bien!!",
            "msg-incorrect": "Te sugerimos repasar el video!!"
        }
    
    ]
}
];    

